<?php

namespace app\models;

use yii\db\ActiveRecord;

class LessonGrammarMetadata extends ActiveRecord
{
    public static function tableName()
    {
        return 'lessonGrammarMetadata';
    }

    public function rules()
    {
        return [
            [['lessonId', 'grammar'], 'required'],
            [['lessonId'], 'integer'],
            [['grammar'], 'string'],
        ];
    }

    public function getLesson()
    {
        return $this->hasOne(LessonList::class, ['lessonNumber' => 'lessonId']);
    }
}