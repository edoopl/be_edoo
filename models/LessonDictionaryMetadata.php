<?php

namespace app\models;

use yii\db\ActiveRecord;

class LessonDictionaryMetadata extends ActiveRecord
{
    public static function tableName()
    {
        return 'lessonDictionaryMetadata';
    }

    public function rules()
    {
        return [
            [['lessonId', 'word', 'translation'], 'required'],
            [['lessonId'], 'integer'],
            [['word', 'translation'], 'string', 'max' => 255],
        ];
    }

    public function getLesson()
    {
        return $this->hasOne(LessonList::class, ['lessonNumber' => 'lessonId']);
    }
}