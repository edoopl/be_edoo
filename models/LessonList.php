<?php

namespace app\models;

use yii\db\ActiveRecord;

class LessonList extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lessonList';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lessonNumber'], 'required'],
            [['lessonNumber'], 'integer'],
        ];
    }
}