<?php

namespace app\models;

use yii\db\ActiveRecord;

class LessonErrorMetadata extends ActiveRecord
{
    public static function tableName()
    {
        return 'lessonErrorMetadata';
    }

    public function rules()
    {
        return [
            [['lessonId', 'wrong', 'correct'], 'required'],
            [['lessonId'], 'integer'],
            [['wrong', 'correct'], 'string', 'max' => 255],
        ];
    }

    public function getLesson()
    {
        return $this->hasOne(LessonList::class, ['lessonNumber' => 'lessonId']);
    }
}