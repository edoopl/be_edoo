<?php

namespace app\controllers;

use yii\rest\Controller;
use yii\web\Response;

class BaseController extends Controller
{
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;

        return $behaviors;
    }
}