<?php

namespace app\controllers;

use app\models\LessonDictionaryMetadata;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;

class DictionaryMetadataController extends BaseController
{
    public function actionCreate(int $lessonId): array
    {
        $model = new LessonDictionaryMetadata();
        $model->lessonId = $lessonId;

        if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {
            Yii::$app->response->statusCode = 201;
            return [
                'content' => 'Created',
                'code' => 201,
                'data' => $model,
            ];
        }
        Yii::$app->response->statusCode = 400;
        return [
            'content' => 'Bad Request',
            'code' => 400,
            'errors' => $model->errors,
        ];
    }

    public function actionView(int $id): LessonDictionaryMetadata
    {
        return $this->findModel($id);
    }

    public function actionUpdate(int $id): array
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post(), '') && $model->save()) {
            return [
                'content' => 'Updated',
                'code' => 200,
                'data' => $model,
            ];
        }
        Yii::$app->response->statusCode = 400;
        return [
            'content' => 'Bad Request',
            'code' => 400,
            'errors' => $model->errors,
        ];
    }

    public function actionDelete(int $id): array
    {
        $model = $this->findModel($id);
        if ($model->delete()) {
            return [
                'content' => 'Deleted',
                'code' => 204,
            ];
        }
        Yii::$app->response->statusCode = 400;
        return [
            'content' => 'Bad Request',
            'code' => 400,
            'errors' => $model->errors,
        ];
    }

    protected function findModel($id): LessonDictionaryMetadata
    {
        if (($model = LessonDictionaryMetadata::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested resource does not exist.');
    }
}