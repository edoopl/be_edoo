<?php

use console\controller\MigrateController;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'SSwGAK_nroAQtXbpaC0X88acyZ_Qnhte',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'enableSession' => false, // Disables sessions
            'loginUrl' => null, // Disables login redirection
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => \yii\symfonymailer\Mailer::class,
            'viewPath' => '@app/mail',
            // send all mails to a file by default.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                'POST   api/lesson/<lessonId:\d+>/dictionary_metadata' => 'dictionary-metadata/create',
                'GET    api/dictionary_metadata/<id:\d+>' => 'dictionary-metadata/view',
                'DELETE api/dictionary_metadata/<id:\d+>' => 'dictionary-metadata/delete',
                'PUT    api/dictionary_metadata/<id:\d+>' => 'dictionary-metadata/update',

                'POST   api/lesson/<lessonId:\d+>/error_metadata' => 'error-metadata/create',
                'GET    api/error_metadata/<id:\d+>' => 'error-metadata/view',
                'DELETE api/error_metadata/<id:\d+>' => 'error-metadata/delete',
                'PUT    api/error_metadata/<id:\d+>' => 'error-metadata/update',


                'POST   api/lesson/<lessonId:\d+>/grammar_metadata' => 'grammar-metadata/create',
                'GET    api/grammar_metadata/<id:\d+>' => 'grammar-metadata/view',
                'DELETE api/grammar_metadata/<id:\d+>' => 'grammar-metadata/delete',
                'PUT    api/grammar_metadata/<id:\d+>' => 'grammar-metadata/update',
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
