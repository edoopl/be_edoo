<?php

use yii\db\Migration;

/**
 * Class m240524_015208_create_dictionary_metadata
 */
class m240524_015208_create_dictionary_metadata extends Migration
{

    public function up()
    {
        $this->createTable('lessonDictionaryMetadata', [
            'id' => $this->primaryKey(),
            'lessonId' => $this->integer()->notNull(),
            'word' => $this->string()->notNull(),
            'translation' => $this->string()->notNull()
        ]);

        $this->addForeignKey(
            'fk-dictionary',
            'lessonDictionaryMetadata',
            'lessonId',
            'lessonList',
            'lessonNumber',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-dictionary',
            'lessonDictionaryMetadata'
        );

        $this->dropTable('lessonDictionaryMetadata');
    }
}
