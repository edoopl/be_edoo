<?php

use yii\db\Migration;

/**
 * Class m240524_020124_create_error_metadata
 */
class m240524_020124_create_error_metadata extends Migration
{

    public function up()
    {
        $this->createTable('lessonErrorMetadata', [
            'id' => $this->primaryKey(),
            'lessonId' => $this->integer()->notNull(),
            'wrong' => $this->string()->notNull(),
            'correct' => $this->string()->notNull()
        ]);

        $this->addForeignKey(
            'fk-error',
            'lessonErrorMetadata',
            'lessonId',
            'lessonList',
            'lessonNumber',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-error',
            'lessonErrorMetadata'
        );

        $this->dropTable('lessonErrorMetadata');
    }
}
