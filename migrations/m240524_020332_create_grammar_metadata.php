<?php

use yii\db\Migration;

/**
 * Class m240524_020332_create_grammar_metadata
 */
class m240524_020332_create_grammar_metadata extends Migration
{
    public function up()
    {
        $this->createTable('lessonGrammarMetadata', [
            'id' => $this->primaryKey(),
            'lessonId' => $this->integer()->notNull(),
            'grammar' => $this->string()->notNull()
        ]);

        $this->addForeignKey(
            'fk-grammar',
            'lessonGrammarMetadata',
            'lessonId',
            'lessonList',
            'lessonNumber',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-grammar',
            'lessonGrammarMetadata'
        );

        $this->dropTable('lessonGrammarMetadata');
    }
}
