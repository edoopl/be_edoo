<?php

use yii\db\Migration;

/**
 * Class m240524_013723_create_lesson_list
 */
class m240524_013723_create_lesson_list extends Migration
{
    public function up()
    {
        $this->createTable('lessonList', [
            'lessonNumber' => $this->primaryKey(),
        ]);

        $this->insert('lessonList', [
            'lessonNumber' => 1,
        ]);
    }

    public function down()
    {
        $this->dropTable('lessonList');
    }
}
